#include QMK_KEYBOARD_H

enum unicode_names {
    ATIL,
    ETIL,
    ITIL,
    OTIL,
    UTIL,
    NTIL,
    MATIL,
    METIL,
    MITIL,
    MOTIL,
    MUTIL,
    MNTIL,
	SUS,
	ALPHA,
	BETA,
	GAMMA,
	DELTA,
	EPSILON,
	ZETA,
	ETA,
	THETA,
	IOTA,
	KAPPA,
	LAMBDA,
	MU,
	NU,
	XI,
	OMICRON,
	PI,
	RHO,
	SIGMA,
	VARSIGMA,
	TAU,
	UPSILON,
	PHI,
	CHI,
	PSI,
	OMEGA,
	MALPHA,
	MBETA,
	MGAMMA,
	MDELTA,
	MEPSILON,
	MZETA,
	META,
	MTHETA,
	MIOTA,
	MKAPPA,
	MLAMBDA,
	MMU,
	MNU,
	MXI,
	MOMICRON,
	MPI,
	MRHO,
	MSIGMA,
	MTAU,
	MUPSILON,
	MPHI,
	MCHI,
	MPSI,
	MOMEGA
};

const uint32_t PROGMEM unicode_map[] = {
    [ATIL] = 0xE1,  // á
    [ETIL] = 0xE9,  // é
    [ITIL] = 0xED,  // í
    [OTIL] = 0xF3,  // ó
    [UTIL] = 0xFA,  // ú
    [NTIL] = 0xF1,  // ñ
    [MATIL] = 0xC1,  // Á
    [METIL] = 0xC9,  // É
    [MITIL] = 0xCD,  // Í
    [MOTIL] = 0xD3,  // Ó
    [MUTIL] = 0xDA,  // Ú
    [MNTIL] = 0xD1,  // Ñ
    [SUS]  = 0x0D9E,  // ඞ
	[ALPHA] = 0x03B1,
	[BETA] = 0x03B2,
	[GAMMA] = 0x03B3,
	[DELTA] = 0x03B4,
	[EPSILON] = 0x03B5,
	[ZETA] = 0x03B6,
	[ETA] = 0x03B7,
	[THETA] = 0x03B8,
	[IOTA] = 0x03B9,
	[KAPPA] = 0x03BA,
	[LAMBDA] = 0x03BB,
	[MU] = 0x03BC,
	[NU] = 0x03BD,
	[XI] = 0x03BE,
	[OMICRON] = 0x03BF,
	[PI] = 0x03C0,
	[RHO] = 0x03C1,
	[SIGMA] = 0x03C3,
	[VARSIGMA] = 0x03C2,
	[TAU] = 0x03C4,
	[UPSILON] = 0x03C5,
	[PHI] = 0x03C6,
	[CHI] = 0x03C7,
	[PSI] = 0x03C8,
	[OMEGA] = 0x03C9,
	[MALPHA] = 0x0391,
	[MBETA] = 0x0392,
	[MGAMMA] = 0x0393,
	[MDELTA] = 0x0394,
	[MEPSILON] = 0x0395,
	[MZETA] = 0x0396,
	[META] = 0x0397,
	[MTHETA] = 0x0398,
	[MIOTA] = 0x0399,
	[MKAPPA] = 0x039A,
	[MLAMBDA] = 0x039B,
	[MMU] = 0x039C,
	[MNU] = 0x039D,
	[MXI] = 0x039E,
	[MOMICRON] = 0x039F,
	[MPI] = 0x03A0,
	[MRHO] = 0x03A1,
	[MSIGMA] = 0x03A3,
	[MTAU] = 0x03A4,
	[MUPSILON] = 0x03A5,
	[MPHI] = 0x03A6,
	[MCHI] = 0x03A7,
	[MPSI] = 0x03A8,
	[MOMEGA] = 0x03A9,
};

enum layer_number {
  _QWERTY = 0,
  _LOWER,
  _RAISE,
  _ADJUST,
  _ARROWS,
  _NUMPAD,
  _GREEK,
  _GAME,
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

/* QWERTY
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |  `   |   1  |   2  |   3  |   4  |   5  |                    |   6  |   7  |   8  |   9  |   0  |  -   |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * | Tab  |   Q  |   W  |   E  |   R  |   T  |                    |   Y  |   U  |   I  |   O  |   P  |  \   |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * | ESC  |   A  |   S  |   D  |   F  |   G  |-------.    ,-------|   H  |   J  |   K  |   L  |   ;  |  BS  |
 * |------+------+------+------+------+------|ARROWS |    |ARROWS |------+------+------+------+------+------|
 * |LShift|   Z  |   X  |   C  |   V  |   B  |-------|    |-------|   N  |   M  |   ,  |   .  |   /  |RShift|
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LAlt | LGUI |LOWER | /Space  /       \Enter \  |RAISE | MENU | RGUI |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */

 [_QWERTY] = LAYOUT(
  KC_GRV,         KC_1,         KC_2,    KC_3,    KC_4,    KC_5,                            KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_MINS,
  LGUI_T(KC_TAB), KC_Q,         KC_W,    KC_E,    KC_R,    KC_T,                            KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    LGUI_T(KC_BSLS),
  CTL_T(KC_ESC),  KC_A,         KC_S,    KC_D,    KC_F,    KC_G,                            KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_BSPC,
  OSM(MOD_LSFT),  C_S_T(KC_Z),  KC_X,    KC_C,    KC_V,    KC_B, OSL(_ARROWS),OSL(_ARROWS), KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, OSM(MOD_RSFT),
                                  KC_LALT, OSL(_ADJUST), OSL(_LOWER), KC_SPC,       CTL_T(KC_ENT), OSL(_RAISE), KC_APP , KC_RALT
),
/* LOWER
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |      |      |      |      |      |      |                    |      |      |      |      |      |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |  F1  |  F2  |  F3  |  F4  |  F5  |  F6  |                    |   +  |   -  |   =  |   [  |   ]  |  \   |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |      |   !  |   @  |   #  |   $  |   %  |-------.    ,-------|   ^  |   &  |   *  |   (  |   )  |   ~  |
 * |------+------+------+------+------+------|   [   |    |    "  |------+------+------+------+------+------|
 * |      |   ?  |      |      |      |      |-------|    |-------|   '  |   _  |   \  |   {  |   }  |   |  |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LAlt | LGUI |LOWER | /Space  /       \Enter \  |NUMPAD|BackSP| RGUI |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */
[_LOWER] = LAYOUT(
  _______, _______, _______, _______, _______, _______,                    _______,      _______, _______, _______, _______, KC_BSPC,
  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,                      KC_PLUS,      KC_MINS, KC_EQL,  KC_LBRC, KC_RBRC, KC_BSLS,
  _______, KC_EXLM, KC_AT,   KC_HASH, KC_DLR,  KC_PERC,                    KC_CIRC,      KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN, KC_TILD,
  _______, KC_QUES, _______, _______, KC_LT,   KC_GT,    _______, _______, KC_DQUO,      KC_UNDS, KC_BSLS, KC_LCBR, KC_RCBR, KC_PIPE,
                             _______, _______, OSL(_GREEK),  _______, _______, _______, _______, _______
),
/* RAISE
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |      |      |      |      |      |      |                    |      |      |      |      |      |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |   F1 |  F2  |  F3  |  F4  |  F5  |  F6  |                    |  F7  |  F8  |  F9  |  F10 |  F11 |  F12 |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |  ESC |   1  |   2  |   3  |   4  |   5  |-------.    ,-------|   6  |   7  |   8  |   9  |   0  |      |
 * |------+------+------+------+------+------|   [   |    |    ]  |------+------+------+------+------+------|
 * |  F7  |   `  |   ~  |  '   |  <   |  >   |-------|    |-------|   +  |   -  |   =  |   [  |   ]  |   \  |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LAlt | LGUI |GREEK | /Space  /       \Enter \  |RAISE |BackSP| RGUI |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 *
 */

[_RAISE] = LAYOUT(
  _______, _______, _______, _______, _______, _______,                            _______, _______, _______, _______, _______, _______,
  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,                         KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,
  _______, KC_1,    KC_2,    KC_3,    KC_4,    KC_5,                          KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    _______,
  KC_F7,   KC_GRV,  KC_TILD, KC_QUOT, KC_LT,   KC_GT,      _______, _______,  KC_PLUS, KC_MINS, KC_EQL,  KC_LBRC, KC_RBRC, KC_BSLS,
                               _______, _______, _______,  _______, _______,  TO(_NUMPAD), _______, _______
),
/* ADJUST
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |      |      |      |      |      |      |                    |      |      |      |      |      |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |      |      |      |      |      |      |                    |      |      |      |      |PRINTS|      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |      |  ඞ   |      |  DEL |      |      |-------.    ,-------|      |      |      |      |      |      |
 * |------+------+------+------+------+------|       |    |       |------+------+------+------+------+------|
 * |      |      |      |      |      |      |-------|    |-------|      |      |      |      |      | GAME |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LAlt | LGUI |LOWER | /Space  /       \Enter \  |RAISE |BackSP| RGUI |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */
  [_ADJUST] = LAYOUT(
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, DF(_QWERTY),
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_PSCR, XXXXXXX,
  XXXXXXX, X(SUS),  XXXXXXX, KC_DEL,  XXXXXXX, XXXXXXX,                   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, TO(_GAME),
                             _______, _______, _______, _______, _______,  _______, _______, _______
  ),

/* ARROWS
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |      |      |      |      |      |      |                    |      |      |      |      |      |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |      |      |      |   É  |      |      |                    |      |  Ú   |  Í   |  Ó   |      |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |      |   Á  |      |      |      |      |-------.    ,-------| LEFT |  UP  | DOWN |RIGHT |      |      |
 * |------+------+------+------+------+------|   '   |    |   "   |------+------+------+------+------+------|
 * |      |      |      |      |      |      |-------|    |-------|  Ñ   |      |      |      |      |      |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LAlt | LGUI |LOWER | /Enter  /       \Enter \  |RAISE |BackSP| RGUI |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */
  [_ARROWS] = LAYOUT(
  XXXXXXX, XXXXXXX,         XXXXXXX, XXXXXXX,         XXXXXXX, XXXXXXX,                   XXXXXXX, XXXXXXX,         XXXXXXX,         XXXXXXX,          XXXXXXX, XXXXXXX,
  XXXXXXX, XXXXXXX,         XXXXXXX, XP(ETIL, METIL), XXXXXXX, XXXXXXX,                   XXXXXXX, XP(UTIL, MUTIL), XP(ITIL, MITIL), XP(OTIL, MOTIL),  XXXXXXX, XXXXXXX,
  XXXXXXX, XP(ATIL, MATIL), XXXXXXX, XXXXXXX,         XXXXXXX, XXXXXXX,                   KC_LEFT, KC_DOWN,         KC_UP,           KC_RIGHT,         XXXXXXX, XXXXXXX,
  KC_RSFT, XXXXXXX,         XXXXXXX, XXXXXXX,         XXXXXXX, XXXXXXX, KC_QUOT, KC_DQUO, XP(NTIL, MNTIL),          XXXXXXX,         XXXXXXX, XXXXXXX, XXXXXXX, KC_LSFT,
                                            _______, _______, _______, KC_ENT,    _______,  _______, _______, _______
  ),
/* NUMPAD
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |      |      |      |      |      |      |                    |      |      |  /   |  *   |   -  |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |      |      |      |      |      |      |                    |      |   7  |  8   |  9   |   +  |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |      |      |      |      |      |      |-------.    ,-------|      |   4  |  5   |  6   |  BS  |      |
 * |------+------+------+------+------+------|       |    |       |------+------+------+------+------+------|
 * |      |      |      |      |      |      |-------|    |-------|      |   1  |  2   |  3   |Enter |      |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LAlt | LGUI |LOWER | /Space  /       \  0   \  |QWERTY|   .  |   ,  |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */
  [_NUMPAD] = LAYOUT(
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                       XXXXXXX, XXXXXXX, KC_SLSH, KC_ASTR, KC_MINS, XXXXXXX,
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                       XXXXXXX, KC_7,    KC_8,    KC_9,    KC_PLUS, XXXXXXX,
  _______, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                       XXXXXXX, KC_4,    KC_5,    KC_6,    KC_BSPC, XXXXXXX,
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,     XXXXXXX, XXXXXXX, XXXXXXX, KC_1,    KC_2,    KC_3,    KC_ENT,  XXXXXXX,
                                 _______, _______, _______, _______, KC_0,    TO(_QWERTY), KC_DOT,  KC_COMM
  ),
/* GREEK
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |   `  |  1   |  2   |  3   |  4   |  5   |                    |  6   |   7  |  8   |  9   |  0   |  -   |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |      |   ς  |  Ω   |  Ε   |  Ρ   |  Τ   |                    |  Υ   |   Θ  |  Ι   |  Ο   |  Π   |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |      |   Α  |  Σ   |  Δ   |  Φ   |  Γ   |-------.    ,-------|  Η   |   Ξ  |  Κ   |  Λ   |      |      |
 * |------+------+------+------+------+------|       |    |       |------+------+------+------+------+------|
 * |      |   Ζ  |  Χ   |  Ψ   |      |  Β   |-------|    |-------|  Ν   |   Μ  |      |      |      |      |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LAlt | LGUI |LOWER | /Space  /       \  0   \  |RAISE |BackSP| RGUI |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */
  [_GREEK] = LAYOUT(
  _______, _______, _______, _______, _______, _______,                       _______, _______, _______, _______, _______, _______,
  XXXXXXX, X(VARSIGMA), XP(OMEGA, MOMEGA), XP(EPSILON, MEPSILON), XP(RHO, MRHO), XP(TAU, MTAU),         XP(UPSILON, MUPSILON), XP(THETA, MTHETA), XP(IOTA, MIOTA), XP(OMICRON, MOMICRON), XP(PI, MPI), _______,
  XXXXXXX, XP(ALPHA, MALPHA), XP(SIGMA, MSIGMA), XP(DELTA, MDELTA), XP(PHI, MPHI), XP(GAMMA, MGAMMA),   XP(ETA, META), XP(XI, MXI), XP(KAPPA, MKAPPA), XP(LAMBDA, MLAMBDA), _______, _______,
  _______, XP(ZETA, MZETA), XP(CHI, MCHI), XP(PSI, MPSI), _______, XP(BETA, MBETA),     XXXXXXX, XXXXXXX, XP(NU, MNU), XP(MU, MMU), _______, _______, _______, _______,
                             _______, _______, TG(_GREEK),  _______, KC_0, _______, _______, _______
  ),
/* GAME
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * | LGUI |   `  |   1  |   2  |   3  |   4  |                    |      |      |      |      |      |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * | Tab  | LALT |   Q  |   W  |   E  |   R  |                    |   7  |   8  |   9  |      |  UP  |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * | CAPS |  ESC |   A  |   S  |   D  |   F  |-------.    ,-------|   4  |   5  |   6  | LEFT | DOWN |RIGHT |
 * |------+------+------+------+------+------| Enter |    |ADJUST |------+------+------+------+------+------|
 * |LCTRL |LShift|   Z  |   X  |   C  |   V  |-------|    |-------|   1  |   2  |   3  |      |      |      |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   |   T  |   G  |  B   | /Space  /       \  BS  \  |   0  |   .  |   ,  |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */

 [_GAME] = LAYOUT(
  KC_LGUI, KC_GRV,  KC_1, KC_2, KC_3, KC_4,                            XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
  KC_TAB,  KC_LALT, KC_Q, KC_W, KC_E, KC_R,                            KC_7,    KC_8,    KC_9,    XXXXXXX, KC_UP,   XXXXXXX,
  KC_CAPS, KC_ESC,  KC_A, KC_S, KC_D, KC_F,                            KC_4,    KC_5,    KC_6,    KC_LEFT, KC_DOWN, KC_RIGHT,
  KC_LCTL, KC_LSFT, KC_Z, KC_X, KC_C, KC_V, KC_ENT,      TO(_QWERTY),  KC_1,    KC_2,    KC_3,    XXXXXXX, XXXXXXX, XXXXXXX,
                      KC_T, KC_G, KC_B,   KC_SPC,          KC_BSPC, KC_0, KC_DOT, KC_COMM
  ),
};

/* layer_state_t layer_state_set_user(layer_state_t state) { */
/*   return update_tri_layer_state(state, _LOWER, _RAISE, _ADJUST); */
/* } */

//SSD1306 OLED update loop, make sure to enable OLED_ENABLE=yes in rules.mk
#ifdef OLED_ENABLE

oled_rotation_t oled_init_user(oled_rotation_t rotation) {
  if (!is_keyboard_master())
    return OLED_ROTATION_180;  // flips the display 180 degrees if offhand
  return rotation;
}

// When you add source files to SRC in rules.mk, you can use functions.
const char *read_layer_state(void);
const char *read_logo(void);
void set_keylog(uint16_t keycode, keyrecord_t *record);
const char *read_keylog(void);
const char *read_keylogs(void);

// const char *read_mode_icon(bool swap);
// const char *read_host_led_state(void);
// void set_timelog(void);
// const char *read_timelog(void);

bool oled_task_user(void) {
  if (is_keyboard_master()) {
    // If you want to change the display of OLED, you need to change here
    oled_write_ln(read_layer_state(), false);
    oled_write_ln(read_keylog(), false);
    oled_write_ln(read_keylogs(), false);
    //oled_write_ln(read_mode_icon(keymap_config.swap_lalt_lgui), false);
    //oled_write_ln(read_host_led_state(), false);
    //oled_write_ln(read_timelog(), false);
  } else {
    oled_write(read_logo(), false);
  }
    return false;
}
#endif // OLED_ENABLE

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  if (record->event.pressed) {
#ifdef OLED_ENABLE
    set_keylog(keycode, record);
#endif
    // set_timelog();
  }
  return true;
}

bool encoder_update_user(uint8_t index, bool clockwise) {
	if (clockwise)
		tap_code(KC_WH_U);
	else
		tap_code(KC_WH_D);

	return false;
}

